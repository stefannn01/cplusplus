#include "BaseConverterNumbers.h"

std::istream& operator>>(std::istream& is, BaseConverterNumbers& bc)
{
	is >> bc.number >> bc.fromBase >> bc.toBase;
	return is;
}

std::ostream& operator<<(std::ostream& os, const BaseConverterNumbers& bc)
{
	os << "Number: " << bc.number << "From base: " << bc.fromBase <<"To base: "<<bc.toBase <<" = " << bc.result;

	return os;
}

BaseConverterNumbers::BaseConverterNumbers(const std::string number, const int fromBase, const int toBase)
{
	this->number = number;
	this->fromBase = fromBase;
	this->toBase = toBase;
	this->result = "";
}

BaseConverterNumbers::BaseConverterNumbers()
{
}

BaseConverterNumbers::BaseConverterNumbers(const BaseConverterNumbers& other)
{
	*this = other;
}

BaseConverterNumbers& BaseConverterNumbers::operator=(const BaseConverterNumbers& other)
{
	this->number = other.number;
	this->fromBase = other.fromBase;
	this->toBase = other.toBase;
	this->result = other.result;
	return *this;
}
