#pragma once
#include<iostream>
#include<string>
class BaseConverterNumbers
{

public:
	BaseConverterNumbers(const std::string number, const int fromBase, const int toBase);
	BaseConverterNumbers();
	BaseConverterNumbers(const BaseConverterNumbers& other);
	BaseConverterNumbers& operator=(const BaseConverterNumbers& other);

public:
	friend std::istream& operator>>(std::istream& is, BaseConverterNumbers& bc);
	friend std::ostream& operator<<(std::ostream& os, const BaseConverterNumbers& bc);


public:
	std::string number;
	int fromBase;
	int toBase;
	std::string result;
};


