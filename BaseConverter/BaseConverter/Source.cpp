#include<iostream>
#include<time.h>
#include<fstream>
#include<stdlib.h>
#include <thread>
#include"BaseConverterNumbers.h"

const int size = 5000000;
const int half_size = 2500000;

void thread1(BaseConverterNumbers *numbers)
{
	char buffer[_MAX_U64TOSTR_BASE2_COUNT];

	for (int i = 0; i < half_size; ++i)
	{
		int number = std::stoi(numbers[i].number, 0, numbers[i].fromBase);
		_itoa_s(number, buffer, numbers[i].toBase);
		numbers[i].result.assign(buffer);
	}
}

void thread2(BaseConverterNumbers* numbers)
{
	char buffer[_MAX_U64TOSTR_BASE2_COUNT];

	for (int i = half_size; i < size; ++i)
	{
		int number = std::stoi(numbers[i].number, 0, numbers[i].fromBase);
		_itoa_s(number, buffer, numbers[i].toBase);
		numbers[i].result.assign(buffer);
	}
}

int main()
{
	std::ifstream numbersFile("ChangeBaseNumbers.txt");
	std::ofstream convertedNumbers("ConvertedNumbers.txt");
	BaseConverterNumbers *numbers = new BaseConverterNumbers[size];
	clock_t time;

	//Reading
	std::cout << "Reading..." << std::endl;
	time = clock();
	for (int i = 0; i < size; ++i)
	{
		numbersFile >> numbers[i];
	}
	time = clock() - time;

	std::cout << "Reading done in: " << ((float)time) / CLOCKS_PER_SEC << std::endl;

	
	//Converting
	time = clock();
	std::cout << "Converting..." << std::endl;

	//VARIANTA FARA THREAD => ~= 5 secunde

	//char buffer[_MAX_U64TOSTR_BASE2_COUNT];

	//for (int i = 0; i < size; ++i)
	//{
	//	int number = std::stoi(numbers[i].number, 0, numbers[i].fromBase);
	//	_itoa_s(number, buffer, numbers[i].toBase);
	//	numbers[i].result.assign(buffer);
	//}


	std::thread t1(thread1, numbers);
	std::thread t2(thread2, numbers);
	t1.join();
	t2.join();
	time = clock() - time;

	std::cout << "Converting done in: " << ((float)time) / CLOCKS_PER_SEC << std::endl;


	//Writing
	std::cout<<"Writing..."<<std::endl;
	time = clock();
	std::cout << "Writing..." << std::endl;
	for (int i = 0; i < size; ++i)
	{
		convertedNumbers << numbers[i].result<<std::endl;
	}
	time = clock() - time;
	std::cout << "Writing done in: " << ((float)time) / CLOCKS_PER_SEC << std::endl;



	return 0;
}